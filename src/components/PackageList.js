
import React, { Component } from "react";

import "../App.css";

class PackageList extends Component {
    state = {
        list_data: [],
        filter: "",
        is_filtering: false
    };

    backend_url = process.env.REACT_APP_BACKEND;

    componentDidMount() {
        this.getPackages()
            .then(res => this.setState({ list_data: res.packages,
                                         filter: this.state.filter,
                                         is_filtering: this.state.is_filtering
                                       }));
    }

    getPackages = async () => {
        console.log("Fetching: " + this.backend_url);
        const response = await fetch(this.backend_url);
        const body = await response.json();

        if (response.status !== 200) throw Error(body.message);

        return body;
    }

    refreshPackages = async() => {
        const response = await fetch(this.backend_url + "/refresh");
        const body = await response.json();

        if (response.status !== 200) throw Error(body.message);

        return body;
    }

    handleRefresh() {
        console.log("Refreshing packages...");
        this.refreshPackages()
            .then(res => this.setState( { list_data: res.packages }));
    }

    handleSearchChange(event) {
        let value = event.target.value;

        if (value.length === 0) {
            this.setState({
                list_data: this.state.list_data,
                filter: "",
                is_filtering: false
            })
        } else {
            this.setState({
                list_data: this.state.list_data,
                filter: value,
                is_filtering: true
            })
        }
    }

    render() {
        let backend_url = process.env.REACT_APP_BACKEND;

        let list_view;

        if (this.state.is_filtering) {
            list_view = (<ul>
                            {this.state.list_data.filter(item => item.includes(this.state.filter))
                                                  .map((item, index) => (
                            <li key={item}>
                                <a href={"/packages/package/" + item}>{item}</a>
                            </li>
                            ))}
                        </ul>
                        )
        } else {
            list_view = (<ul>
                            {this.state.list_data.map((item, index) => (
                                <li>
                                    <a href={"/packages/package/" + item}>
                                        {item}
                                    </a>
                                </li>
                            ))}
                         </ul>)
        }

        return (
            <header className="package-list">
                <h1>Packages:</h1>

                <p>Backend at: {backend_url}</p>

                <span>
                    <button class="button" onClick={this.handleRefresh.bind(this)}>Refresh packages</button>
                    <label>Search: </label>
                    <input type="text" name="search" onChange={this.handleSearchChange.bind(this)}></input>
                </span>

                {list_view}
            </header>
        );
    }
}

export default PackageList;
