
import React, { Component } from "react";
import { useParams } from "react-router-dom";

import "../App.css";

class Package extends Component {
    state = {
      package: null,
      loaded: false
    }

    backend_url = process.env.REACT_APP_BACKEND;

    componentDidMount() {
        let pkg = this.props.pkg;
        this.getPackage(pkg)
            .then(res => this.setState({ package: res,
                                         loaded: true
                                       }));
    }

    async getPackage(pkg) {
        const resp = await fetch(this.backend_url + "/package/" + pkg);
        const body = await resp.json();

        if (resp.status !== 200) throw Error(body.message);

        return body;
    }

    render() {
      if (this.state.loaded === false) {
        return (
          <div>
            <p>Loading...</p>
          </div>
        )
      }

      if (this.state.package == null) {
        return (
          <div>
            <h2>Package not found</h2>
            <a href="/packages">Back to package list</a>
          </div>
        )
      }

      return(
            <header className="package">
              <a href="/packages">Back to package list</a>
              <h2>{this.state.package.name}</h2>
                <p>
                  {this.state.package.description}
                </p>

              <h3>Dependencies:</h3>
              <ul>
                {this.state.package.dependencies.map((item, index) => (
                  
                    <li key={item}>
                        <a href={"/packages/package/" + item}>
                          {item}
                        </a>
                    </li>
                ))}
              </ul>


              <h3>Reverse Dependencies:</h3>
              <ul>
                {this.state.package.reverse_dependencies.map((item, index) => (
                    <li key={item}>
                      <a href={"/packages/package/" + item}>
                        {item}
                      </a>
                    </li>
                ))}
              </ul>
            </header>
        )
    }
}

export default Package;
