import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams
} from "react-router-dom";

import logo from './logo.svg';
import './App.css';

import PackageList from "./components/PackageList";
import Package from "./components/Package";

function App(props) {
  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <Switch>
            <Route path="/packages/package/:pkg" children={<PackageContainer/>}/>

            <Route exact_path="/">
              <PackageList backend={props.backend}/>
            </Route>

            <Route exact_path="/packages">
              <PackageList backend={props.backend}/>
            </Route>
          </Switch>
        </header>
      </div>
    </Router>
  );
}

// This component exists only to wrap
// a Package component to make useParams() work
// react-router-dom is weird... :/
function PackageContainer() {
  let { pkg } = useParams();

  return (
    <div>
      <Package pkg={pkg}/>
    </div>
  );
}

export default App;
