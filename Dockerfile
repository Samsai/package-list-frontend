FROM node:alpine

RUN mkdir -p /usr/src/app
COPY . /usr/src/app
WORKDIR /usr/src/app

RUN npm i

RUN npm install -g serve

EXPOSE 4000

CMD npm run build && serve -s -l 4000 build
